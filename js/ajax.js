var divMidHead = document.getElementById("div-mid-head");
var workList = document.getElementById("works-list");
var worksListAjax = document.getElementById("works-list-ajax");
var worksSection = document.getElementById("works-section");
var bottomMenu = document.getElementById("menu");
var menuXhttp = document.getElementById("menu-ul");
var back = document.getElementById("back");
var me = document.getElementById("meXhttp");
var es = document.getElementById("es");
var contatti = document.getElementById("contatti");
var chiama = document.getElementById("chiama");
var scrivi = document.getElementById("scrivi");
var event;

/* === PASSIVE EVENT LISTENER === */

jQuery.event.special.touchstart = {
	setup: function( _, ns, handle ){
		if ( ns.includes("noPreventDefault") ) {
			this.addEventListener("touchstart", handle, { passive: false });
		} else {
			this.addEventListener("touchstart", handle, { passive: true });
		}
	}
};

jQuery.event.special.onmousewheel = {
	setup: function( _, ns, handle ){
		if ( ns.includes("noPreventDefault") ) {
			this.addEventListener("onmousewheel", handle, { passive: false });
		} else {
			this.addEventListener("onmousewheel", handle, { passive: true });
		}
	}
};
jQuery.event.special.onmousedown = {
	setup: function( _, ns, handle ){
		if ( ns.includes("noPreventDefault") ) {
			this.addEventListener("onmousedown", handle, { passive: false });
		} else {
			this.addEventListener("onmousedown", handle, { passive: true });
		}
	}
};
jQuery.event.special.onmousemove = {
	setup: function( _, ns, handle ){
		if ( ns.includes("noPreventDefault") ) {
			this.addEventListener("onmousemove", handle, { passive: false });
		} else {
			this.addEventListener("onmousemove", handle, { passive: true });
		}
	}
};

window.history.replaceState({state: "/"}, document.title, "/");
document.getElementById("meXhttp").removeAttribute("href");

	//document.title = "Andrea Zangheri";

/* === MOBILE TOUCH - NO-TOUCH === */

$(document).ready(function() {
	if ("ontouchstart" in document.documentElement) {
		$('.no-touch').removeClass('no-touch').addClass('touch')
	}
	$('.touch').on('touchstart touchend', function(e) {
		$(this).toggleClass('over');
	});
})

/* === LOADER === */

$body = $("body");
$error = $("#error");
$(document).on({
	ajaxStart: function() { $body.addClass("loading");},
	ajaxError: function() { $error.removeClass("hide");},
	ajaxStop: function() { $body.removeClass("loading");}
});

/* === RANDOM WORK === 

var myfirsRequest = new XMLHttpRequest();

if (window.XMLHttpRequest) {
		myfirstRequest = new XMLHttpRequest();
	} else {
		myfirstRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}

myfirstRequest.open('GET', '/js/data.json');*/

divMidHead.classList.remove('hide');
workList.classList.remove('hide');
bottomMenu.classList.remove('hide');
es.classList.remove('hide');
contatti.classList.remove('hide');
chiama.classList.remove('hide');
scrivi.classList.remove('hide');
/*
myfirstRequest.onloadend = function() {
	divMidHead.classList.remove('hide');
	bottomMenu.classList.remove('hide');
	workList.classList.remove('hide');
	var data = JSON.parse(myfirstRequest.responseText);
	
		/* === Render Data === 
		
		rendermeHTML(data);
		rendermenuHTML(data);
		renderRandomHTML(data);
	};
	
myfirstRequest.send();

Handlebars.registerHelper('encode', function(context){
		return new Handlebars.SafeString(context);
});

Handlebars.registerHelper('random', function(context, options){
	var e = Math.floor(Math.random() * (context.length)); //numero progetti
	var r = options.fn(context[e]);
	return r;
});
			
/* === ME === */
			
var meEvento = function meEvento(){
	
    $.get('/js/data.json');
	$("#mid-head").empty();
	$("#works").empty();
	$("#works-ajax").empty();
	$("#menu-ul").empty();
	var myRequest = new XMLHttpRequest();
	
	if (window.XMLHttpRequest) {
		myRequest = new XMLHttpRequest();
	} else {
		myRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
			   
	myRequest.open('GET', '/js/data.json');		
	
	myRequest.onerror = function(){
	console.log("Connection error");
	};
			
	myRequest.onloadend = function() {
	/* === AJAX CRAWLER === */
	document.title = "Andrea Zangheri";
	//document.getElementsByTagName('meta')["og:title"].content = "Andrea Zangheri ⋅ " + location.pathname;
	//document.getElementsByTagName('meta')["description"].content = "Web & Multimodal Designer &#8901; Developer [...]";
	//document.getElementsByTagName('meta')["og:description"].content = "Web & Multimodal Designer &#8901; Developer [...]";
	//document.getElementsByTagName('meta')["keywords"].content = "";
	
	window.history.pushState({state: "/"}, document.title, "/");
	
	/* === AJAX CRAWLER === */
		es.classList.remove('hide');
		chiama.classList.remove('hide');
		contatti.classList.remove('hide');
		scrivi.classList.remove('hide');
		var s = myRequest.responseText;
		// preserve newlines, etc - use valid JSON
s = s.replace(/\\n/g, "\\n")  
               .replace(/\\'/g, "\\'")
               .replace(/\\"/g, '\\"')
               .replace(/\\&/g, "\\&")
               .replace(/\\r/g, "\\r")
               .replace(/\\t/g, "\\t")
               .replace(/\\b/g, "\\b")
               .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
s = s.replace(/[\u0000-\u001F]+/g,""); 
var data = JSON.parse(s);
		
		/* === Render Data === */
		
		rendermeHTML(data);
		rendermenuHTML(data);
		renderRandomHTML(data);
		
		/* === Show Content === */
				
		back.classList.remove("show");
		divMidHead.classList.remove("hide");
		bottomMenu.classList.remove("hide");
		workList.classList.remove("hide");
		worksListAjax.classList.remove("show");
		worksListAjax.classList.remove("fixed");
		worksListAjax.classList.remove("relative");
		worksSection.classList.remove("show");
		menuXhttp.classList.remove("hide");
		
		back.classList.add("hide");
		divMidHead.classList.add("show");
		bottomMenu.classList.add("show");
		workList.classList.add("show");
		worksListAjax.classList.add("hide");
		worksSection.classList.add("hide");
		menuXhttp.classList.add("show");
		window.scrollTo(0,0);
		}
				
	myRequest.send();
	
	
Handlebars.registerHelper("encode", function(context){
		return new Handlebars.SafeString(context);
});
	
	Handlebars.registerHelper("random", function(context, options){
	var e = Math.floor(Math.random() * (context.length)); //numero progetti
	var r = options.fn(context[e]);
	return r;
	});
			
	};
			
/* === PROGETTI === */
			
	var progettiEvento = function progettiEvento(){
	
	window.onpopstate = function() {
		window.history.pushState({state: "/"}, document.title, "/");
		meEvento();
	};
		
	es.classList.add('hide');
	chiama.classList.add('hide');
	scrivi.classList.add('hide');
	contatti.classList.add('hide');
    $.get('/js/data.json');
	$("#mid-head").empty();
	$("#works").empty();
	$("#works-ajax").empty();
	$("#menu-ul").empty();
	$('#meXhttp').prop("onclick", null);
	$('#meXhttp').removeAttr("onclick");
	$('#meXhttp').prop("onclick",' meEvento()');
	$('#meXhttp').attr("onclick",' meEvento()');
	$('#back').prop("onclick", null);
	$('#back').removeAttr("onclick");
	$('#back').prop("onclick",' meEvento()');
	$('#back').attr("onclick",' meEvento()');
		
	var xhttp;
	
	if (window.XMLHttpRequest) {
		xhttp = new XMLHttpRequest();
	} else {
		xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xhttp.open("GET", "/js/data.json");
				
	xhttp.onerror = function(){
	console.log("Connection error");
	};
				
  	xhttp.send();
				
	xhttp.onloadend = function(){	
		
		/* === AJAX CRAWLER === */
	document.title = "Andrea Zangheri ⋅ " + "Progetti";
	//document.getElementsByTagName('meta')["og:title"].content = "Andrea Zangheri ⋅ " + location.pathname;
	document.getElementsByTagName('meta')["description"].content = "Portfolio lavori e progetti";
	//document.getElementsByTagName('meta')["og:description"].content = "Portfolio lavori e progetti";
		
		
	window.history.pushState({state: '/progetti.html'}, document.title, '/progetti.html');
		
		/* === AJAX CRAWLER === */
					
		var s = xhttp.responseText;
		// preserve newlines, etc - use valid JSON
s = s.replace(/\\n/g, "\\n")  
               .replace(/\\'/g, "\\'")
               .replace(/\\"/g, '\\"')
               .replace(/\\&/g, "\\&")
               .replace(/\\r/g, "\\r")
               .replace(/\\t/g, "\\t")
               .replace(/\\b/g, "\\b")
               .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
s = s.replace(/[\u0000-\u001F]+/g,""); 
var data = JSON.parse(s);
					
		/* === Render Data === */
		
	renderHTML(data);
		
		/* === Show Content === */
				
	back.classList.remove("hide");
	divMidHead.classList.remove("show");
	bottomMenu.classList.remove("show");
	workList.classList.remove("show");
	worksListAjax.classList.remove("hide");
	worksListAjax.classList.remove("fixed");
	worksSection.classList.remove("show");
				
	back.classList.add("show");
	divMidHead.classList.add("hide");
	bottomMenu.classList.add("hide");
	workList.classList.add("hide");
	worksListAjax.classList.add("show");
	worksListAjax.classList.add("relative");
	worksSection.classList.add("hide");
	window.scrollTo(0,0);
	}

};
			
/* === LAVORI === */

var lavoriEvento = function lavoriEvento(event){
	
	window.onpopstate = function() {
		progettiEvento();
		window.history.pushState({state: "/progetti.html"}, document.title, "/progetti.html");
		//console.log(JSON.stringify(event.state).replace(/{|{|state|:|"|}/g, ""));
		//console.log(location.pathname);
	};
	
	es.classList.add('hide');
	chiama.classList.add('hide');
	scrivi.classList.add('hide');
	contatti.classList.add('hide');
	
	if( !event ) event = window.event;
back.classList.remove("show");
back.classList.add("hide");
$.get('/js/data.json');		
$("#works-ajax").empty();
$('#meXhttp').prop("onclick", null);
$('#meXhttp').removeAttr("onclick");
$('#meXhttp').prop("onclick", 'progettiEvento()');
$('#meXhttp').attr("onclick", 'progettiEvento()');
$('#back').prop("onclick", null);
$('#back').removeAttr("onclick");
$('#back').prop("onclick", 'progettiEvento()');
$('#back').attr("onclick", 'progettiEvento()');
var xhttp;
var currentID = event.target.id -1 || event.srcElement.id -1;
//var currentAnno = event.target.id; // Connesso ad HandleBars Register Helper "custom"
console.log(currentID);
var currentProgetto = event.target.textContent;
var lavoroStr = currentProgetto.replace(/\s+/g, '-').toLowerCase();
	console.log(lavoroStr);
	
	worksSection.classList.add("hide");
	back.classList.remove("hide");
	
if (window.XMLHttpRequest) {
	xhttp = new XMLHttpRequest();
} else {
	xhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
	xhttp.open("GET", "/js/data.json");		
	
	xhttp.onerror = function(){
	console.log("Connection error");
	};
			
	xhttp.onloadend = function(){
		
	/* === AJAX CRAWLER === */
	
	document.title = "Andrea Zangheri ⋅ " + "Progetti ⋅ " + currentProgetto;
	////document.getElementsByTagName('meta')["og:title"].content = "Andrea Zangheri ⋅ " + location.pathname;
	document.getElementsByTagName('meta')["description"].content = currentProgetto + " ⋅ " + "descrizione progetto JSON" ;
	////document.getElementsByTagName('meta')["og:description"].content = "Portfolio lavori e progetti";
	//
	window.history.pushState({state: "/progetti/" + lavoroStr + ".html"}, document.title, "progetti/" + lavoroStr + ".html");
	
	/* === AJAX CRAWLER === */
		
		/* === Parse Data === */
		
		var s = xhttp.responseText;
		// preserve newlines, etc - use valid JSON
s = s.replace(/\\n/g, "\\n")  
               .replace(/\\'/g, "\\'")
               .replace(/\\"/g, '\\"')
               .replace(/\\&/g, "\\&")
               .replace(/\\r/g, "\\r")
               .replace(/\\t/g, "\\t")
               .replace(/\\b/g, "\\b")
               .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
s = s.replace(/[\u0000-\u001F]+/g,""); 
var data = JSON.parse(s);
		renderNextHTML(data);
		var i;
		var imgContainer = document.querySelector('#image-container');
		var container = imgContainer.querySelectorAll('.container');
		
		for ( i = 0; i < container.length; i++){
		container[i].classList.add('is-loading');
		//console.log(container);
		}
		for ( i = 0; i < container.length; i++){
		var img = container[i].querySelectorAll('img');
		img[0].classList.add('hide');
		//console.log(img);
		}
		
		new imagesLoaded(imgContainer, function(instance) {
			for (i = 0; i < container.length; i++ ) {
			container[i].classList.remove('is-loading');
			var img = container[i].querySelectorAll('img');
			img[0].classList.remove('hide');
			img[0].classList.add('show');
			}
		});
	
		worksSection.classList.remove('hide');
		//worksListAjax.classList.remove("relative");
		worksListAjax.classList.remove('show');
		divMidHead.classList.remove('show');
		workList.classList.remove('show');
		menuXhttp.classList.remove('show');
	
		//worksListAjax.classList.add("fixed");
		worksSection.classList.add('show');
		worksListAjax.classList.add('hide');
		divMidHead.classList.add('hide');
		workList.classList.add('hide');
		menuXhttp.classList.add('hide');
		window.scrollTo(0,0);
};
	
	xhttp.send();
	//La posizione del lavoro nel file json determina il risultato dell'helper 
	Handlebars.registerHelper("custom", function(context, options){
		var r = options.fn(context[currentID]);
		return r;
		console.log(currentID);
	});
}
			
function renderHTML(data) {
	var rawTemplate = document.getElementById("progetti-template").innerHTML;
	var compiledTemplate= Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(data);
				
	var worksAjax = document.getElementById("works-ajax");
	worksAjax.innerHTML = ourGeneratedHTML;
}	

function rendermeHTML(data) {
	var rawTemplate = document.getElementById("me-template").innerHTML;
	var compiledTemplate= Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(data);
				
	var meAjax = document.getElementById("mid-head");
	meAjax.innerHTML = ourGeneratedHTML;
}
	
function renderRandomHTML(data) {
	var rawTemplate = document.getElementById("random-template").innerHTML;
	var compiledTemplate= Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(data);
				
	var randomAjax = document.getElementById("works");
	randomAjax.innerHTML = ourGeneratedHTML;
}
			
function rendermenuHTML(data) {
	var rawTemplate = document.getElementById("menu-template").innerHTML;
	var compiledTemplate= Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(data);
				
	var menuAjax = document.getElementById("menu-ul");
	menuAjax.innerHTML = ourGeneratedHTML;
}

function renderNextHTML(data) {
	var rawTemplate = document.getElementById("works-template").innerHTML;
	var compiledTemplate= Handlebars.compile(rawTemplate);
	var ourGeneratedHTML = compiledTemplate(data);
				
	var workSection = document.getElementById("works-section");
	workSection.innerHTML = ourGeneratedHTML;
}